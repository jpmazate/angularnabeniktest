import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Movie } from './model/movie';
import { ConnectService } from './services/connect.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-nabenik-test';
  //guardar
  idBuscarPelicula = '';
  tituloGuardar = '';
  anioGuardar = '';
  duracionGuardar = '';

  //Editar
  idEditar = '';
  tituloEditar = '';
  duracionEditar = '';
  anioEditar = '';
  
  //Eliminar
  idEliminar = '';

  constructor(private conectService: ConnectService){

  }

  ngOnInit(): void {
    
  }

  getAll(){
    this.conectService.findAllMovies().subscribe(
      (response:Array<Movie>) => {
        alert(response)
        console.log(response)
      },
      (error) => {
        alert(error)
        console.log(error)
      }
    );
  }

  getSpecificMovie(){
    this.conectService.findSpecificMovie(this.idBuscarPelicula).subscribe(
      (response) => {
        alert(response)
        console.log(response)
      },
      (error) => {
        alert(error)
        console.log(error)
      }
    );

  }

  saveMovie(){
    var movie = new Movie(this.tituloGuardar, this.anioGuardar, this.duracionGuardar);
    this.conectService.saveMovie(movie).subscribe(
      (response) => {
        alert(response)
        console.log(response)
      },
      (error) => {
        alert(error)
        console.log(error)
      }
    );
  }

  editMovie(){
    var movie = new Movie(this.tituloEditar, this.anioEditar, this.duracionEditar, +this.idEditar);
    this.conectService.editMovie(+this.idEditar, movie).subscribe(
      (response) => {
        alert(response)
        console.log(response)
      },
      (error) => {
        alert(error)
        console.log(error)
      }
    );
  }

  deleteMovie(){
    this.conectService.deleteMovie(+this.idEliminar).subscribe(
      (response) => {
        alert(response)
        console.log(response)
      },
      (error) => {
        alert(error)
        console.log(error)
      }
    );
  }





}

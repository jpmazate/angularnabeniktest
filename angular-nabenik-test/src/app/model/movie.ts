export class Movie {
    id?:Number;
    title?: string;
    year?: string;
    duration?:string;

    constructor(title:string, year:string, duration:string,id?:Number){
        this.id = id;
        this.title = title;
        this.year = year;
        this.duration = duration;
    }

    

}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from '../model/movie';

@Injectable({
  providedIn: 'root'
})
export class ConnectService {

  editURL = 'http://localhost:8080/back-test/rest/movies';
  saveURL = 'http://localhost:8080/back-test/rest/movies';
  findAllURL = 'http://localhost:8080/back-test/rest/movies?title=';
  findSpecificURL = 'http://localhost:8080/back-test/rest/movies';
  deleteURL = 'http://localhost:8080/back-test/rest/movies';
  constructor(private _http: HttpClient) { }

  findAllMovies(): Observable<Movie[]>{
    var url = this.findAllURL;
    return  this._http.get<Movie[]>(url);
  }

  findSpecificMovie(id: string):Observable<Movie>{
    var url = this.findSpecificURL + "/"+id;
    return  this._http.get<Movie>(url);
  }

  saveMovie(movie: Movie):Observable<Movie>{
    return this._http.post<Movie>(this.saveURL,movie);
  }

  editMovie(id: number, movie: Movie): Observable<any> {
    return this._http.put<any>(this.editURL+"/"+id,movie)
  
  }
  deleteMovie(id:number): Observable<any> {
    return this._http.delete<any>(this.deleteURL+"/"+id)
}
}
